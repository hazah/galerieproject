-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 15 Mars 2020 à 15:24
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `galeriedb`
--

-- --------------------------------------------------------

--
-- Structure de la table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_name` varchar(255) DEFAULT NULL,
  `SHARED` tinyint(1) DEFAULT '0',
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`album_id`),
  KEY `FK_ALBUM_userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `album`
--

INSERT INTO `album` (`album_id`, `album_name`, `SHARED`, `userid`) VALUES
(1, 'Famille', 1, 1),
(2, 'Amis', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `IMAGEID` int(11) NOT NULL AUTO_INCREMENT,
  `CREATED` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `HEIGTH` int(11) DEFAULT NULL,
  `image_path` longtext,
  `MODIFIED` datetime DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `WIDTH` int(11) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`IMAGEID`),
  KEY `FK_IMAGE_userid` (`userid`),
  KEY `FK_IMAGE_album_id` (`album_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`IMAGEID`, `CREATED`, `DESCRIPTION`, `HEIGTH`, `image_path`, `MODIFIED`, `TITLE`, `WIDTH`, `album_id`, `userid`) VALUES
(1, '2020-03-07 00:40:49', 'Famille', 12, 'moustapha.jpg', NULL, 'moustapha', 5, 1, 1),
(2, '2020-03-07 00:45:29', 'Famille', 12, 'moi.jpg', NULL, 'moustapha', 7, 1, 1),
(3, '2020-03-07 01:32:18', 'Famille', 8, 'mon petit.jpg', NULL, 'jhonattan', 5, 1, 1),
(4, '2020-03-07 01:50:10', 'Famille', 12, 'mon_fils.jpg', NULL, 'jhonattan', 11, 1, 1),
(5, '2020-03-07 01:51:19', 'Famille', 12, 'copain.jpg', NULL, 'beMen', 12, 2, 2),
(6, '2020-03-07 02:00:15', 'Amitié', 1, 'bMen', NULL, 'beMen© ', 2, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `USERID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `ROLE` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`USERID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`USERID`, `EMAIL`, `PASSWORD`, `ROLE`, `USERNAME`) VALUES
(1, 'detafisco@gmail.com', 'passer', 'admin', 'tafa'),
(2, 'cheikhahmadoubamba@gmail.com', 'bamba', 'user', 'bamba');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `FK_ALBUM_userid` FOREIGN KEY (`userid`) REFERENCES `user` (`USERID`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FK_IMAGE_album_id` FOREIGN KEY (`album_id`) REFERENCES `album` (`album_id`),
  ADD CONSTRAINT `FK_IMAGE_userid` FOREIGN KEY (`userid`) REFERENCES `user` (`USERID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
